package com.example.a.mynewapplication;

public class Task {

    private String Titel;
    private String Subject;
    private String Text;
    private String Date;
    private String Stars;
    private String Status;

    public Task() {
    }

    //Construactor//

    public Task(String titel, String subject, String text, String date, String stars, String status) {
        Titel = titel;
        Subject = subject;
        Text = text;
        Date = date;
        Stars = stars;
        Status = status;
    }

    //setter and getter//

    public String getTitel() {
        return Titel;
    }

    public void setTitel(String titel) {
        Titel = titel;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }

    public String getText() {
        return Text;
    }

    public void setText(String text) {
        Text = text;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getStars() {
        return Stars;
    }

    public void setStars(String stars) {
        Stars = stars;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }
}
