package com.example.a.mynewapplication;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class TaskActivity extends Activity {

    // fields
    private FirebaseDatabase mdatabase;
    private DatabaseReference mDBRef;
    Button insert;
    Button Cancel;
    EditText Titel,Subject,Text,Date,Stars,Status;
    Task task;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);


        //initialize the Firebase instance
        mdatabase = FirebaseDatabase.getInstance();
        mDBRef = mdatabase.getReference("Task");
        task = new Task();

        Titel = (EditText)findViewById(R.id.Titel);
        Subject = (EditText)findViewById(R.id.Subject);
        Text = (EditText) findViewById(R.id.Text);
        Date = (EditText)findViewById(R.id.Date);
        Stars =(EditText)findViewById(R.id.Stars);
        Status =(EditText)findViewById(R.id.Status);

        Cancel = (Button)findViewById(R.id.btnCancel) ;
        insert = (Button) findViewById(R.id.btninsert);

    }

    private void getValues(){
        task.setTitel(Titel.getText().toString());
        task.setSubject(Subject.getText().toString());
        task.setText(Text.getText().toString());
        task.setDate(Date.getText().toString());
        task.setStars(Stars.getText().toString());
        task.setStatus(Status.getText().toString());

    }


    public void btnInsert(View view){
        mDBRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                getValues();
                mDBRef.child("Task").setValue(task);
                Toast.makeText(TaskActivity.this,"Data Insetr",Toast.LENGTH_LONG).show();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}

